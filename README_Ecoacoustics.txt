#Process the recordings to extract species information (--lat 51.97757 --lon 5.71877). See example below:

Getting data at highest sensitivity for 3 second intervals
python analyze.py --i ../datasets/NL_FieldRecordings_Ommurdetuin/Data_red_tag_first_farm/20230509-20230521_SIM_A/  --lat 51.97757 --lon 5.71877 --o results/NL_FieldRecordings_Ommurdetuin/Data_red_tag_first_farm --threads 18 --sensitivity 1.5 --batchsize 18



#Generate the species list corresponding to Ommurdetuin area, just to know:

  python species.py --o example/ommurdertuin_species_list.txt --lat 51.97757 --lon 5.71877
193 species list generated
Species list stored at: example/ommurdertuin_species_list.txt

