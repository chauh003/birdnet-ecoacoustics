import os
import pandas as pd

# Specify the directory containing the CSV files
directory_path = '/home/aneesh/sandbox/ecoacoustics/BirdNET-Analyzer/results/NL_FieldRecordings_Ommurdetuin/Data_red_tag_first_farm'

def load_csvs_from_directory(directory):
    csv_files = [file for file in os.listdir(directory) if file.endswith('.csv')]
    dataframes = []
    for file in csv_files:
        file_path = os.path.join(directory, file)
        df = pd.read_csv(file_path)
        dataframes.append(df)
    return dataframes


# Call the function to load CSV files from the directory
csv_dataframes = load_csvs_from_directory(directory_path)

# Process the loaded dataframes as needed
for df in csv_dataframes:
    print(df.head())  # Example: print the first few rows of each dataframe

