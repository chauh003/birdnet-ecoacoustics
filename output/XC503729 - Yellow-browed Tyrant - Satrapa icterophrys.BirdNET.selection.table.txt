Selection	View	Channel	Begin Time (s)	End Time (s)	Low Freq (Hz)	High Freq (Hz)	Species Code	Common Name	Confidence
1	Spectrogram 1	1	6.0	9.0	150	12000	sieela3	Sierran Elaenia	0.4629
2	Spectrogram 1	1	6.0	9.0	150	12000	lottyr1	Long-tailed Tyrant	0.2182
3	Spectrogram 1	1	12.0	15.0	150	12000	sieela3	Sierran Elaenia	0.1891
4	Spectrogram 1	1	15.0	18.0	150	12000	houwre	House Wren	0.4450
5	Spectrogram 1	1	18.0	21.0	150	12000	sieela3	Sierran Elaenia	0.6759
6	Spectrogram 1	1	18.0	21.0	150	12000	azaspi1	Azara's Spinetail	0.1883
7	Spectrogram 1	1	18.0	21.0	150	12000	lasfly	La Sagra's Flycatcher	0.1653
8	Spectrogram 1	1	18.0	21.0	150	12000	blbtre1	Black-billed Treehunter	0.1040
